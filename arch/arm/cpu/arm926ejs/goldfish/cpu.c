#include <common.h>
#include <linux/errno.h>
#include <asm/io.h>
#include <linux/compiler.h>

/* Lowlevel init isn't used on i.MX28, so just have a dummy here */
__weak void lowlevel_init(void) {}

#if defined(CONFIG_DISPLAY_CPUINFO)

int print_cpuinfo(void)
{
    printf("CPU:    Goldfish Enulator (based on arm926ejs)\n");
    return 0;
}

#endif