#ifndef _CONFIG_GOLDFISH_H
#define _CONFIG_GOLDFISH_H

#include <linux/sizes.h>

#define CONFIG_ENV_SIZE             8192
#define CONFIG_SYS_LOAD_ADDR        0x7fc0

#define CONFIG_NR_DRAM_BANKS        1
#define CONFIG_SYS_MAX_FLASH_BANKS  1
#define CONFIG_SYS_MALLOC_LEN       (10 * SZ_1M)
#define PHYS_SDRAM                  0x00000000
#define PHYS_SDRAM_SIZE             (512 * SZ_1M)
#define PHYS_FLASH_SIZE             (64 * SZ_1M)
#define CONFIG_SYS_SDRAM_BASE       PHYS_SDRAM

#define CONFIG_SYS_INIT_RAM_ADDR    0x00800000
#define CONFIG_SYS_INIT_RAM_SIZE    0x000FFFFF
#define CONFIG_SYS_GBL_DATA_OFFSET  (CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR     (CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_GBL_DATA_OFFSET)

/*
 * Where in virtual memory the IO devices (timers, system controllers
 * and so on)
 */
#define IO_BASE			0xfe000000                 // VA of IO 
#define IO_SIZE			0x00800000                 // How much?
#define IO_START		0xff000000                 // PA of IO

#define GOLDFISH_INTERRUPT_BASE         (0x0)
#define GOLDFISH_INTERRUPT_STATUS       (0x00) // number of pending interrupts
#define GOLDFISH_INTERRUPT_NUMBER       (0x04)
#define GOLDFISH_INTERRUPT_DISABLE_ALL  (0x08)
#define GOLDFISH_INTERRUPT_DISABLE      (0x0c)
#define GOLDFISH_INTERRUPT_ENABLE       (0x10)

#define GOLDFISH_PDEV_BUS_BASE          (0x1000)
#define GOLDFISH_PDEV_BUS_END           (0x100)

#define GOLDFISH_TTY_BASE               (0x2000)
#define GOLDFISH_TIMER_BASE             (0x3000)
#define GOLDFISH_TTY1_BASE              (0x11000)
#define GOLDFISH_TTY2_BASE              (0x12000)

#define IO_ADDRESS(x) ((x) + IO_START)

#endif /* _CONFIG_GOLDFISH_H */