/******************************************************************************
 *
 * serial_goldfish.c - U-Boot serial port implementation for goldfish
 *
 * Copyright (c) 2013 Roger Ye.  All rights reserved.
 * Software License Agreement
 *
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
 * NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
 * NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. The AUTHOR SHALL NOT, UNDER
 * ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 * DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *****************************************************************************/
#ifndef __BARE_METAL__
#include <common.h>
#include <serial.h>
#include <configs/goldfish.h>
#else
#include <bsp.h>
#define NULL 0
#include <hardware.h>
#endif

#include "serial_goldfish.h"

/*
 * There are a few UART in goldfish. UART 0, UART 1 and UART 2.
 * When emulator is running with argument -shell, UART 2 is used as standard input/output device.
 * UART base addresses:
 * 0 - 0xff002000
 * 1 - 0xff011000
 * 2 - 0xff012000
 * */


static int goldfish_init(void)
{
	writel(GOLDFISH_TTY_CMD_INT_ENABLE, (void *)IO_ADDRESS(GOLDFISH_TTY2_BASE) + GOLDFISH_TTY_CMD);
	return 0;
}

static int goldfish_disable_tty(void)
{
	writel(GOLDFISH_TTY_CMD_INT_DISABLE, (void *)IO_ADDRESS(GOLDFISH_TTY2_BASE) + GOLDFISH_TTY_CMD);
	return 0;
}

static void goldfish_putc(const char c)
{
    writel(c, IO_ADDRESS(GOLDFISH_TTY2_BASE) + GOLDFISH_TTY_PUT_CHAR);
}

static int goldfish_tstc(void)
{
    int bytesReady;
	bytesReady = readl(IO_ADDRESS(GOLDFISH_TTY2_BASE) + GOLDFISH_TTY_BYTES_READY);

	return bytesReady;
}

static int goldfish_getc(void)
{

    int retval;
    int bytesReady;

    while((bytesReady = goldfish_tstc()) < 1) {
    }

    writel((int)&retval, IO_ADDRESS(GOLDFISH_TTY2_BASE) + GOLDFISH_TTY_DATA_PTR);
    writel(1, IO_ADDRESS(GOLDFISH_TTY2_BASE) + GOLDFISH_TTY_DATA_LEN);
    writel(GOLDFISH_TTY_CMD_READ_BUFFER, IO_ADDRESS(GOLDFISH_TTY2_BASE) + GOLDFISH_TTY_CMD);
	
	return retval;
}

static void goldfish_setbrg(void)
{
	debug("goldfish_setbrg()\n");
}

static struct serial_device goldfish_drv = {
	.name	= "goldfish_serial",
	.start	= goldfish_init,
	.stop	= goldfish_disable_tty,
	.setbrg	= goldfish_setbrg,
	.putc	= goldfish_putc,
	.puts	= default_serial_puts,
	.getc	= goldfish_getc,
	.tstc	= goldfish_tstc,
};

void goldfish_serial_initialize(void)
{
	serial_register(&goldfish_drv);
}

__weak struct serial_device *default_serial_console(void)
{
	return &goldfish_drv;
}

#ifdef CONFIG_DM_SERIAL

U_BOOT_DRIVER(serial_goldfish) = {
    .name = "serial_goldfish",
    .id = UCLASS_SERIAL,
    .probe = goldfish_serial_probe,
    .ops = &goldfish_serial_ops,

};

#endif // CONFIG_DM_SERIAL


